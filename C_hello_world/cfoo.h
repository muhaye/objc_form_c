//
//  cfoo.h
//  C_hello_world
//
//  Created by Ngendo Muhayimana on 2018-02-18.
//  Copyright © 2018 Ngendo Muhayimana. All rights reserved.
//

#ifndef cfoo_h
#define cfoo_h

#include <pthread.h>

void *thread(void *arg);

#endif /* cfoo_h */

//
//  ViewController.h
//  C_hello_world
//
//  Created by Ngendo Muhayimana on 2018-02-18.
//  Copyright © 2018 Ngendo Muhayimana. All rights reserved.
//

#ifdef __OBJC__

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *middleText;

@end

#endif

void showHello(char *in_text);

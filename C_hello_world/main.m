//
//  main.m
//  C_hello_world
//
//  Created by Ngendo Muhayimana on 2018-02-18.
//  Copyright © 2018 Ngendo Muhayimana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "cfoo.h"
#import "ViewController.h"
#import <pthread.h>

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        pthread_t pt;
        pthread_create(&pt, NULL, thread, NULL);
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}



//
//  ViewController.m
//  C_hello_world
//
//  Created by Ngendo Muhayimana on 2018-02-18.
//  Copyright © 2018 Ngendo Muhayimana. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

ViewController *this_vc;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    this_vc = self;
}

void showHello(char *in_text) {
    dispatch_async(dispatch_get_main_queue(), ^{
        this_vc.middleText.text = @(in_text);
    });
    printf("%s", in_text);
}
@end

//
//  cfoo.c
//  C_hello_world
//
//  Created by Ngendo Muhayimana on 2018-02-18.
//  Copyright © 2018 Ngendo Muhayimana. All rights reserved.
//

#include "cfoo.h"
#include "ViewController.h"
#include <pthread.h>
#include <unistd.h> // for sleep() and usleep()

void *thread(void *arg) { // arguments not used in this case
    sleep(4); // wait 5 seconds
    usleep(10000); // wait 10000 microseconds (1000000s are 1 second)
    // thread has sleeped for 4.01 seconds
    char *hw = "Hello World";
    showHello(hw); // call your function
    // add more here
    return NULL;
}

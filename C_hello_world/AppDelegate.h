//
//  AppDelegate.h
//  C_hello_world
//
//  Created by Ngendo Muhayimana on 2018-02-18.
//  Copyright © 2018 Ngendo Muhayimana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

